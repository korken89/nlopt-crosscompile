#!/bin/bash

# NLOPT
NLOPT_LINK=https://github.com/stevengj/nlopt/archive/nlopt-2.4.2.tar.gz

# Compiler flags
CPU=cortex-a53
FLAGS="-g -O2 -ftree-vectorize -ffast-math -mcpu=${CPU}"

# Get library
wget ${NLOPT_LINK}
tar -xvf nlopt*.tar.gz

# Prepare for installation
cd nlopt-nlopt*
mkdir tempman templib tempinc
CURR_DIR=$(pwd)

# Generate configuration
./autogen.sh

# Generate AArch64 configuration
./configure --enable-maintainer-mode --with-cxx --with-pic --without-guile --without-python --without-octave --without-matlab --host=aarch64-linux-gnu CFLAGS="${FLAGS}" CXXFLAGS="${FLAGS}" --libdir=${CURR_DIR}/templib --includedir=${CURR_DIR}/tempinc --mandir=${CURR_DIR}/tempman

# Compile and install
make -j
make install

# Copy the lib
mkdir ../include ../lib
cp tempinc/nlopt.h* ../include
cp templib/lib*.a ../lib

# Clean up
cd ..
rm -rf nlopt-*
