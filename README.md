# NLOPT cross compilation script

A simple script that cross compiles NLOPT for AArch64 using the aarch64-linux-gnu
toolchain.

Set the CPU (default Cortex-A53) and run `./build_nlopt_aarch64.sh` and when it
is done the library and includes will be in the `lib` and `include` folders respectively.
